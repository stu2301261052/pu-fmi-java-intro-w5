import java.util.Scanner;

public class Main {

    static class Product {
        String name;
        int quantity;
        String serialNumber;

        public Product(String name, int quantity, String serialNumber) {
            this.name = name;
            this.quantity = quantity;
            this.serialNumber = serialNumber;
        }
    }

    static class User {
        String firstName;
        String lastName;
        int uniqueID;
        int age;

        public User(String firstName, String lastName, int uniqueID, int age) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.uniqueID = uniqueID;
            this.age = age;
        }

        public void printDetails() {
            System.out.println("Добре дошъл в мазето на баба " + firstName + " " + lastName);
            System.out.println("Ти се идентифицира с номер " + uniqueID);
            System.out.println("Ти си на " + age + " години");
        }
    }

    static void printWelcomeMessage() {
        System.out.println("Мазе бот v1 на Вашите услуги");
    }

    static User getUserDetails(Scanner scanner) {
        System.out.print("Въведете вашето първо име: ");
        String firstName = scanner.next();
        System.out.print("Въведете вашето второ име: ");
        String lastName = scanner.next();
        System.out.print("Въведете уникалния си идентификатор (факултетен номер): ");
        int uniqueID = scanner.nextInt();
        System.out.print("Въведете вашата възраст: ");
        int age = scanner.nextInt();

        return new User(firstName, lastName, uniqueID, age);
    }

    static void printInventory(Product[] products) {
        System.out.println("===========================================================");
        System.out.println("|продукти           |брой            |нов сериен номер    |");
        System.out.println("===========================================================");
        for (Product product : products) {
            System.out.printf("|%-16s |%-16d |%-20s |%n", product.name, product.quantity, product.serialNumber);
        }
        System.out.println("===========================================================");
    }

    public static void main(String[] args) {
        printWelcomeMessage();

        Scanner scanner = new Scanner(System.in);

        User user = getUserDetails(scanner);

        Product[] products = {
                new Product("ябълково вино", 10, "C7544_10"),
                new Product("пушено месо", 5, "M7441_5"),
                new Product("сливов мармалад", 9, "S6491_9"),
                new Product("мариновани чушки", 4, "P7485_4"),
                new Product("прасенце касичка", 184, "B6584_184")
        };

        user.printDetails();
        printInventory(products);

        scanner.close();
    }
}
